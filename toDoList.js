
// If an li element is clicked, toggle the class "done" on the <li>
//**Use example from for loop slides**/
const ulItems = document.getElementsByTagName('ul');
for (u = 0; u < ulItems.length; u++) {
// Add event listener to li tag to show completed task using for loop
  const liItems = ulItems[u].getElementsByTagName('li')
  for (i = 0; i < liItems.length; i++) {
    const task = liItems[i];
    liItems[i].addEventListener('click', () => {
      task.classList.toggle('done');
    })
  }
}
//Less Clunky Version 
// Create variable for parent node
//const ulList = document.getElementsByClassName("today-list")[0];

// If an li element is clicked, toggle the class "done" on the <li>
// **Use example from slides**//
//function itemDone() {
//  document.querySelectorAll('li').forEach(item => {
//    item.addEventListener('click', event => {
//      item.className = 'done';
//    })
//  })
//}


// If a delete link is clicked, delete the li element / remove from the DOM
for (u = 0; u < ulItems.length; u++) {
  const liItems = ulItems[u].getElementsByTagName('li')
  const deleteItem = ulItems[u].getElementsByClassName('delete')
  for (i = 0; i < liItems.length; i++) {
    const task = liItems[i];
    deleteItem[i].addEventListener('click', () => {
      task.remove();
    })
  }
}
 
//Less Clunky Version
// If a delete link is clicked, delete the li element / remove from the DOM
//function itemDelete() {
//  document.querySelectoreAll('delete').forEach(item => {
//    item.addEventListener('click', event => { 
//      ulList.removeChild(item.parentNode)
//    })
//  })
//}



// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
//const addListItem = function(e) {
//  e.preventDefault();
//  const input = this.parentNode.getElementsByTagName('input')[0];
//  const text = input.value; // use this text to create a new <li>

const addItemToList = () => {
  const input = document.getElementsByTagName('input')[0]
 
  const text = input.value; // use this text to create a new <li>
  // Create <span> element
  const spanElement = document.createElement('span');
  spanElement.textContent=text;

  //Create <a> element (class and text name)
  const aElement = document.createElement('a');
  aElement.className='delete';
  aElement.textContent='Delete';
  aElement.addEventListener('click', () => {
    aElement.remove();
  })
  
  //Create <li> element and append
  const liElement = document.createElement('li');
  liElement.appendChild(spanElement);
  liElement.appendChild(aElement);
  //Append li to ul
  document.getElementsByTagName('ul')[0].appendChild(liElement);
  //Add event 'done' event listener to span element
  spanElement.addEventListener('click', () => {
    liElement.classList.toggle('done');
  })
  //Add 'remove' listener
  aElement.addEventListener('click', () => {
    liElement.remove();
  })
  
  
  // Finish function here

};
// Add item to array list
// How do I add a space between the delete listener and list item??
const addItem = document.getElementsByClassName('add-item');
Array.from(addItem).forEach(function(element) {
  element.addEventListener('click', addItemToList);
});

