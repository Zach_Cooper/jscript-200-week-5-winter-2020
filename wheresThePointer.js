// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click

const tbody = document.querySelector('tbody');
tbody.addEventListener('click', function(e) {
    const td = e.target;
    td.innerText = `${e.clientX}, ${e.clientY}`;
    
})
